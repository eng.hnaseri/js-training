module.exports = {
    parser: 'babel-eslint',
    env: {
      browser: true,
    },
    extends: 'airbnb',
    plugins: [
      'react'
    ],
    // add your custom rules here
    'rules': {
      'import/extensions': 0,
      'no-console': process.env.NODE_ENV === 'production' ? 2 : 0,
      'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,
      'no-alert': process.env.NODE_ENV === 'production' ? 2 : 0,
      'consistent-return': 0,
      'no-useless-escape': 0,
      'no-useless-return': 0,
      'no-restricted-properties': 0,
      'no-underscore-dangle': 0,
      'no-duplicate-imports': 0,
      'no-confusing-arrow': 0,
      'arrow-parens': 0,
      'require-await': 2,
      'import/no-unresolved': 0,
      'no-useless-constructor': 0,
      'comma-dangle': [ 2, 'never'],
      'indent': [ 2, 4, { 'SwitchCase': 1 }],
      'import/no-extraneous-dependencies': 0,
      'no-useless-rename': 0,
      'class-methods-use-this': 0,
      'no-prototype-builtins': 0,
      'no-mixed-operators': 0,
      'no-plusplus': 0,
      'operator-assignment': 0,
      'no-lonely-if': 0,
      'no-unexpected-multiline': 0,
      'space-unary-ops': 0,
      'import/prefer-default-export': 0,
      'import/newline-after-import': 0,
      'no-use-before-define': 0,
      'react/jsx-indent-props': [ 2, 4 ],
      "react/jsx-equals-spacing": [2, "never"],
      'react/jsx-indent': [ 2, 4 ],
      "react/forbid-prop-types": 0,
      "react/no-find-dom-node": 0,
      "react/no-string-refs": 0,
      "react/jsx-closing-bracket-location": [
          0,
          { "selfClosing": "after-props", "nonEmpty": "after-props" }
        ],
      "react/jsx-first-prop-new-line": 0,
      "react/jsx-space-before-closing": 0,
      "react/jsx-filename-extension": 0,
      "react/require-default-props": 0,
      "jsx-a11y/no-static-element-interactions": 0,
      "camelcase": 0
    },
    "globals": {
      "it": true,
      "expect": true,
      "describe": true,
      "jest": true
    }
  }